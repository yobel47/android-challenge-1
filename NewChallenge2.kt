package com.example.tes

fun formatNumber(phone: String): String{

    // write your code here
    var newPhone: String
    Regex("[^A-Za-z0-9]").apply {
        newPhone = replace(phone, "")
    }
    if(newPhone.first().toString() == "0"){
        newPhone = newPhone.replaceFirstChar { "62" }
    }
    return newPhone
}

fun main() {
    println(formatNumber(readLine1()))
    println(formatNumber(readLine2()))
    println(formatNumber(readLine3()))
}

//input 1
fun readLine1() = "0851-6231-7243"

//input 2
fun readLine2() = "0877 6431 7123"

//input 3
fun readLine3() = "+62877 6294 2312"
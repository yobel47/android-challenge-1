package com.example.tes

fun isAnagram(a: String, b: String): Boolean{
    if(a.length != b.length){
        return false
    }

    val arr1 = a.toCharArray()
    val arr2 = b.toCharArray()

    arr1.sort()
    arr2.sort()

    val str1 = String(arr1)
    val str2 = String(arr2)

    return str1 == str2
}

fun main() {
    println(isAnagram("mycar", "camry"))
    println(isAnagram("Hello", "hello"))
    println(isAnagram("anagram", "margana"))
    println(isAnagram("Raden", "Denah"))
}

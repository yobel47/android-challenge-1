package com.example.tes

open class Segitiga(){
    var x = 0
    private var k = 0
    open fun show(){
        for(i in 1..x){
            for (space in 1..x - i) {
                print(" ")
            }

            while (k != 2 * i - 1) {
                print("*")
                ++k
            }

            println()
            k = 0
        }
    }

}

class SegitigaTerbalik(){
    var x = 0
    fun show(){
        for (i in x downTo 1) {
            for (j in 1..x - i) {
                print(" ")
            }
            for (j in i until 2 * i) {
                print("*")
            }
            for (j in 0 until i - 1) {
                print("*")
            }
            println()
        }
    }
}

class Kristal:Segitiga(){
    var g = 1
    fun showKristal(){
        super.show()
        for (i in 1..x) {
            for (k in 1..g){
                print(" ")
            }
            g++
            for (j in 1 until 2*(x-i)){
                print("*")
            }
            println()
        }
    }

}

class Ex(){
    var x = 0
    fun show(){
        for(i in 1..x){
            for(j in 1..x){
                if (j == i || j == (x + 1 - i))
                    print("*");
                else
                    print(" ")
            }
            println()
        }
    }
}

class SegitigaBolong(){
    var x = 0
    fun show(){
        for(i in 1..x){
            for(j in i..x){
                print(" ")
            }
            if(i==1 || i==x){
                for(k in 1 until i*2){
                    print("*")
                }
            }else{
                for(j in 1 until i*2){
                    if(j==1 || j==i*2-1){
                        print("*")
                    }else{
                        print(" ")
                    }
                }
            }
            println()
        }
    }
}

fun main() {
    val segitiga = Segitiga()
    segitiga.x = 6
    segitiga.show()
    println()

    val segitigaTerbalik = SegitigaTerbalik()
    segitigaTerbalik.x = 6
    segitigaTerbalik.show()
    println()

    val kristal = Kristal()
    kristal.x = 6
    kristal.showKristal()
    println()

    val ex = Ex()
    ex.x = 6
    ex.show()
    println()

    val segitigaBolong = SegitigaBolong()
    segitigaBolong.x = 6
    segitigaBolong.show()
    println()
}

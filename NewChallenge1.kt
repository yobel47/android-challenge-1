package com.example.tes

fun arrayChallenge(arr: Array<Int>): Int {

    // write your code here
    var x = -1
    for(i in arr.indices){
        for (j in i+1 until arr.size){
            if(x < arr[j]-arr[i]){
                x = arr[j]-arr[i]
            }
        }
    }
    return x;
}

fun main() {
    println(arrayChallenge(readLine1()))
    println(arrayChallenge(readLine2()))
}

//input 1
fun readLine1(): Array<Int> = arrayOf(44, 30, 24, 32, 35, 30, 40, 38, 15)

//input 2
fun readLine2(): Array<Int> = arrayOf(10, 9, 8, 2)